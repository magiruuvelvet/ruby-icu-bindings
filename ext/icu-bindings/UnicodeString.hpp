#ifndef UNICODESTRING_HPP
#define UNICODESTRING_HPP

#include "Char.hpp"
#include "Vector.hpp"
#include "List.hpp"

#include <type_traits>

class UnicodeString;

using UnicodeStringVector = Vector<UnicodeString>;
using UnicodeStringList = List<UnicodeString>;

// convenience string wrapper around ICU
class UnicodeString
{
    // internal type aliases
    using char_type = Char;
    using type = Vector<char_type>;
    using raw_type = std::vector<char_type::type>;

public:
    using size_type = type::size_type;
    using index = type::index;

    // error code for various search methods
    static const index npos = -1;

    UnicodeString();
    UnicodeString(const UnicodeString&);
    UnicodeString(const std::vector<Char> &utf32chars);
    UnicodeString(const std::vector<Char::type> &utf32cp);
    UnicodeString(const std::string &utf8);
    UnicodeString(const char *utf8);
    ~UnicodeString();

    // copy assignment operators
    UnicodeString &operator= (const UnicodeString&);
    UnicodeString &operator= (const std::vector<Char> &utf32chars);
    UnicodeString &operator= (const std::vector<Char::type> &utf32cp);
    UnicodeString &operator= (const std::string &utf8);
    UnicodeString &operator= (const char *utf8);

    // operator*, direct access to raw data -> *myString
    inline const char_type *operator*() const
    { return dd.data(); }

    enum CaseSensitivity {
        CaseSensitive,       // default
        CaseInsensitive,
    };

    enum SplitMode {
        KeepEmptyParts,         // default, keeps all parts
        SkipEmptyParts,         // skips empty() parts
        TrimAndSkipEmptyParts,  // trims substring before empty() check
    };

    // compare operators (always case sensitive!!)
    inline bool operator== (const UnicodeString &other) const
    { return compare_data(other.dd); }
    inline bool operator== (const std::string &utf8) const
    { return compare_data(from_utf8(utf8).dd); }
    inline bool operator== (const char *utf8) const
    { return compare_data(from_utf8(utf8).dd); }

    // compare methods with configurable case sensitivity
    static bool compare(const UnicodeString &l, const UnicodeString &r, CaseSensitivity cs = CaseSensitive);
    inline bool compare(const UnicodeString &other, CaseSensitivity cs = CaseSensitive) const
    { return compare(*this, other, cs); }

    inline bool operator!= (const UnicodeString &other) const
    { return !compare_data(other.dd); }
    inline bool operator!= (const std::string &utf8) const
    { return !compare_data(from_utf8(utf8).dd); }
    inline bool operator!= (const char *utf8) const
    { return !compare_data(from_utf8(utf8).dd); }

    bool operator< (const UnicodeString &other)  const;
    inline bool operator< (const std::string &utf8) const
    { return operator< (from_utf8(utf8)); }
    inline bool operator< (const char *utf8) const
    { return operator< (from_utf8(utf8)); }

    bool operator<= (const UnicodeString &other) const;
    inline bool operator<= (const std::string &utf8) const
    { return operator<= (from_utf8(utf8)); }
    inline bool operator<= (const char *utf8) const
    { return operator<= (from_utf8(utf8)); }

    bool operator> (const UnicodeString &other) const;
    inline bool operator> (const std::string &utf8) const
    { return operator> (from_utf8(utf8)); }
    inline bool operator> (const char *utf8) const
    { return operator> (from_utf8(utf8)); }

    bool operator>= (const UnicodeString &other) const;
    inline bool operator>= (const std::string &utf8) const
    { return operator>= (from_utf8(utf8)); }
    inline bool operator>= (const char *utf8) const
    { return operator>= (from_utf8(utf8)); }

    // append operators
    inline UnicodeString &operator+= (const UnicodeString &other)
    { append(other); return *this; }
    inline UnicodeString &operator+= (const std::string &utf8)
    { append(utf8); return *this; }
    inline UnicodeString &operator+= (const char *utf8)
    { append(utf8); return *this; }
    inline UnicodeString &operator+= (const char ascii)
    { append(ascii); return *this; }
    inline UnicodeString &operator+= (const Char::type &utf32cp)
    { append(utf32cp); return *this; }
    inline UnicodeString &operator+= (const Char &utf32char)
    { append(utf32char); return *this; }

    // concatenate operators
    inline UnicodeString operator+ (const UnicodeString &other) const
    { auto self = *this; self.append(other); return self; }
    inline UnicodeString operator+ (const std::string &utf8) const
    { auto self = *this; self.append(utf8); return self; }
    inline UnicodeString operator+ (const char *utf8) const
    { auto self = *this; self.append(utf8); return self; }
    inline UnicodeString operator+ (const char ascii) const
    { auto self = *this; self.append(ascii); return self; }
    inline UnicodeString operator+ (const Char::type &utf32cp) const
    { auto self = *this; self.append(utf32cp); return self; }
    inline UnicodeString operator+ (const Char &utf32char) const
    { auto self = *this; self.append(utf32char); return self; }

    // convert string to UTF-8
    std::string to_utf8() const;
    static inline std::string to_utf8(const UnicodeString &str)
    { return str.to_utf8(); }
    inline operator std::string() const
    { return to_utf8(); }

    // convert string to UTF-16
    std::vector<Char::utf16_t> to_utf16() const;
    static inline std::vector<Char::utf16_t> to_utf16(const UnicodeString &str)
    { return str.to_utf16(); }

    // convert string from UTF-8
    static UnicodeString from_utf8(const std::string &utf8);

    // convert string from UTF-16
    static UnicodeString from_utf16(const std::vector<Char::utf16_t> &utf16);
    static UnicodeString from_utf16(const Char::utf16_t *utf16array, size_t len);

    //
    // forward iterators of internal UTF-32 data vector
    //
    inline auto begin()          { return dd.begin(); }
    inline auto begin()    const { return dd.begin(); }
    inline auto end()            { return dd.end(); }
    inline auto end()      const { return dd.end(); }
    inline auto cbegin()   const { return dd.cbegin(); }
    inline auto cend()     const { return dd.cend(); }
    inline auto rbegin()         { return dd.rbegin(); }
    inline auto rbegin()   const { return dd.rbegin(); }
    inline auto rend()           { return dd.rend(); }
    inline auto rend()     const { return dd.rend(); }
    inline auto crbegin()  const { return dd.crbegin(); }
    inline auto crend()    const { return dd.crend(); }
    inline auto front()          { return dd.front(); }
    inline auto front()    const { return dd.front(); }
    inline auto back()           { return dd.back(); }
    inline auto back()     const { return dd.back(); }

    inline auto erase(type::const_iterator pos) { return dd.erase(pos); }
    inline auto erase(type::const_iterator first, type::const_iterator last)
    { return dd.erase(first, last); }

    inline auto resize(size_type new_size) { return dd.resize(new_size); }
    inline auto resize(size_type new_size, const type::value_type &x)
    { return dd.resize(new_size, x); }

    // size in UTF-32 code points
    inline auto size()     const { return dd.size(); }
    inline auto length()   const { return dd.size(); }

    // empty check
    inline auto empty()    const { return dd.empty(); }
    inline auto isEmpty()  const { return dd.empty(); }

    // position in UTF-32 code points
    inline auto at(type::size_type n) { return dd.at(n); }
    inline auto at(type::size_type n) const { return dd.at(n); }
    inline auto operator[] (type::size_type n) { return dd[n]; }
    inline auto operator[] (type::size_type n) const { return dd[n]; }

    // UTF-32 data array
    inline auto data()     const { return dd.data(); }

    // clear UnicodeString object
    inline auto clear()          { return dd.clear(); }

    // find position of character or substring
    // returns npos if nothing was found
    index find(const Char &ch, size_t from = 0, CaseSensitivity cs = CaseSensitive) const;
    index find(const UnicodeString &substr, size_t from = 0, CaseSensitivity cs = CaseSensitive) const;
    index find_last(const Char &ch, size_t from = 0, CaseSensitivity cs = CaseSensitive) const;
    index find_last(const UnicodeString &substr, size_t from = 0, CaseSensitivity cs = CaseSensitive) const;

    // index_of - alias for find(); basically the same
    inline index index_of(const Char &ch, size_t from = 0, CaseSensitivity cs = CaseSensitive) const
    { return find(ch, from, cs); }
    inline index index_of(const UnicodeString &substr, size_t from = 0, CaseSensitivity cs = CaseSensitive) const
    { return find(substr, from, cs); }

    // last_index_of - alias for find_last(); basically the same
    inline index last_index_of(const Char &ch, size_t from = 0, CaseSensitivity cs = CaseSensitive) const
    { return find_last(ch, from, cs); }
    inline index last_index_of(const UnicodeString &substr, size_t from = 0, CaseSensitivity cs = CaseSensitive) const
    { return find_last(substr, from, cs); }

    // fill entire string with a character
    UnicodeString &fill(const Char &ch);
    inline UnicodeString &fill(const Char::type &cp) { fill(Char(cp)); return *this; }
    inline UnicodeString &fill(const char ascii) { fill(Char(ascii)); return *this; }

    // resize and fill entire string with a character
    UnicodeString &fill(const Char &ch, size_t size);
    inline UnicodeString &fill(const Char::type &cp, size_t size) { fill(Char(cp), size); return *this; }
    inline UnicodeString &fill(const char ascii, size_t size) { fill(Char(ascii), size); return *this; }

    // append characters
    inline UnicodeString &append(const char ascii)
    { dd.emplace_back(ascii); return *this; }
    inline UnicodeString &append(const Char::type &utf32cp)
    { dd.emplace_back(utf32cp); return *this; }
    inline UnicodeString &append(const Char &utf32char)
    { dd.emplace_back(utf32char); return *this; }

    // prepend characters
    inline UnicodeString &prepend(const char ascii)
    { dd.insert(dd.begin(), ascii); return *this; }
    inline UnicodeString &prepend(const Char::type &utf32cp)
    { dd.insert(dd.begin(), utf32cp); return *this; }
    inline UnicodeString &prepend(const Char &utf32char)
    { dd.insert(dd.begin(), utf32char); return *this; }

    // append strings
    inline UnicodeString &append(const UnicodeString &other)
    { for (auto&& c : other)
          append(c);
      return *this; }
    inline UnicodeString &append(const std::string &utf8)
    { for (auto&& c : from_utf8(utf8))
          append(c);
      return *this; }
    inline UnicodeString &append(const char *utf8)
    { append(std::string(utf8)); return *this; }

    // prepend strings
    inline UnicodeString &prepend(const UnicodeString &other)
    { auto pos = 0;
      for (auto&& c : other)
      {
          dd.insert(dd.begin() + pos, c);
          ++pos;
      }
      return *this;
    }
    inline UnicodeString &prepend(const std::string &utf8)
    { auto pos = 0;
      for (auto&& c : from_utf8(utf8))
      {
          dd.insert(dd.begin() + pos, c);
          ++pos;
      }
      return *this;
    }
    inline UnicodeString &prepend(const char *utf8)
    { prepend(std::string(utf8)); return *this; }

    //
    // manipulate case sensitivity
    //
    UnicodeString &to_lower();
    UnicodeString lower() const;
    UnicodeString &to_upper();
    UnicodeString upper() const;

    //
    // trim whitespaces from string
    //
    UnicodeString &trim();
    UnicodeString &trim_front();
    UnicodeString &trim_back();
    UnicodeString trimmed() const;
    UnicodeString trimmed_front() const;
    UnicodeString trimmed_back() const;

    //
    // simplify string: trim and remove inner repeating whitespaces
    //
    UnicodeString &simplify();
    UnicodeString simplified() const;

    //
    // convert from number
    // only supports literal types (uses type_traits)
    //
    template<typename _NumberType, typename = std::enable_if<std::is_literal_type<_NumberType>::value>>
    static inline UnicodeString number(const _NumberType &num)
    { return UnicodeString(std::to_string(num)); }

    // starts with substring
    bool starts_with(const UnicodeString &ss, CaseSensitivity cs = CaseSensitive) const;
    inline bool starts_with(const char ascii, CaseSensitivity cs = CaseSensitive) const
    { return starts_with(UnicodeString() + ascii, cs); }
    inline bool not_starts_with(const UnicodeString &ss, CaseSensitivity cs = CaseSensitive) const
    { return !starts_with(ss, cs); }
    inline bool not_starts_with(const char ascii, CaseSensitivity cs = CaseSensitive) const
    { return !starts_with(ascii, cs); }

    // ends with substring
    bool ends_with(const UnicodeString &ss, CaseSensitivity cs = CaseSensitive) const;
    inline bool ends_with(const char ascii, CaseSensitivity cs = CaseSensitive) const
    { return ends_with(UnicodeString() + ascii, cs); }
    inline bool not_ends_with(const UnicodeString &ss, CaseSensitivity cs = CaseSensitive)
    { return !ends_with(ss, cs); }
    inline bool not_ends_with(const char ascii, CaseSensitivity cs = CaseSensitive) const
    { return !ends_with(ascii, cs); }

    //
    // split string at delimiter
    //
    UnicodeStringVector split(const Char sep, SplitMode = KeepEmptyParts) const;
    UnicodeStringVector split(const UnicodeString &sep, SplitMode = KeepEmptyParts) const;

    //
    // replace methods
    //
    inline UnicodeString &replace(size_type i, size_type len, Char after)
    { replace(i, len, UnicodeString().fill(after, 1)); return *this; }
    UnicodeString &replace(size_type i, size_type len, const UnicodeString &after);
    inline UnicodeString &replace(Char before, Char after)
    { replace_helper(*this, UnicodeString().fill(before, 1), UnicodeString().fill(after, 1)); return *this; }
    inline UnicodeString &replace(const UnicodeString &before, const UnicodeString &after)
    { replace_helper(*this, before, after); return *this; }
    inline UnicodeString &replace(Char before, const UnicodeString &after)
    { replace_helper(*this, UnicodeString().fill(before, 1), after); return *this; }
    inline UnicodeString &replace(const UnicodeString &before, Char after)
    { replace_helper(*this, before, UnicodeString().fill(after, 1)); return *this; }

    //
    // remove methods
    //
    inline UnicodeString &remove(size_type i, size_type len)
    { return this->replace(i, len, UnicodeString()); }
    inline UnicodeString &remove(Char c)
    { return this->replace(c, UnicodeString()); }
    inline UnicodeString &remove(const UnicodeString &s)
    { return this->replace(s, UnicodeString()); }

    //
    // get substring
    //  npos == to end()
    //  throws std::out_of_range when requested values were too high
    //
    UnicodeString mid(size_type pos, index len = npos) const;
    inline UnicodeString substr(size_type pos, index len = npos) const
    { return mid(pos, len); }

    //
    // number conversion methods
    //
    int toInt(bool *ok = nullptr) const;
    inline int32_t toInt32(bool *ok = nullptr) const
    { return toInt(ok); }

    uint32_t toUInt(bool *ok = nullptr) const;
    inline uint32_t toUInt32(bool *ok = nullptr) const
    { return toUInt(ok); }

    int64_t toInt64(bool *ok = nullptr) const;

    uint64_t toULongLong(bool *ok = nullptr) const;
    inline uint64_t toUInt64(bool *ok = nullptr) const
    { return toULongLong(ok); }

    float toFloat(bool *ok = nullptr) const;
    double toDouble(bool *ok = nullptr) const;
    long double toLongDouble(bool *ok = nullptr) const;

private:
    // internal UTF-32 data
    type dd;
    const raw_type internal_data() const;               // read-only copy <char_type::type>
    bool compare_data(const type &other) const;

    static UnicodeString from_uchar32_buffer_static(const uint32_t *buf, size_t len);
    UnicodeString &from_uchar32_buffer(const uint32_t *buf, size_t len);

    // internal compare helper
    bool compare_helper(size_t start,
                        size_t length,
                        const UnicodeString &srcText,
                        size_t srcStart,
                        size_t srcLength) const;

    // internal replace helper
    static void replace_helper(UnicodeString &str, const UnicodeString &from, const UnicodeString &to);

    // sum of all chars for bitwise operations
    char_type::type calculate_char_total() const;
};

#endif // UNICODESTRING_HPP
