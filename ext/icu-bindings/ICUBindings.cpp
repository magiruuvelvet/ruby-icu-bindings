#include "rice/rice.hpp"

#include "Char.hpp"
#include "UnicodeString.hpp"

#include <ruby/encoding.h>

// creates an Ruby UTF-8 String object
// Rice::String doesn't support encodings at all and defaults to ASCII
// which returns \x-escaped garbage on Unicode input
VALUE MakeRubyUTF8String(const char* s)
{
    VALUE str_val = rb_str_new2(s);
    static int enc_index = rb_enc_find_index("UTF-8");
    rb_enc_associate_index(str_val, enc_index);
    return str_val;
}

typedef bool (*icu_callback)(const int32_t&);
typedef Char (*icu_set_callback)(const int32_t&);
typedef const std::string (*icu_sym_callback)(const int32_t&);

inline const int32_t object_to_codepoint(const Rice::Object &obj)
{
    const auto cp = obj.iv_get("@codepoint");
    return static_cast<int32_t>(NUM2INT(cp));
}

inline Rice::Object icu_ret(const Rice::Object &obj, icu_callback callback)
{
    auto b = callback(object_to_codepoint(obj));
    return b ? to_ruby(true) : to_ruby(false);
}

inline Rice::Object icu_char_set(Rice::Object &self, icu_set_callback callback)
{
    auto c = callback(object_to_codepoint(self));
    self.iv_set("@codepoint", c());
    return self;
}

inline Rice::Object icu_get_sym(const Rice::Object &obj, icu_sym_callback callback)
{
    auto cp = object_to_codepoint(obj);
    UnicodeString sym(callback(cp));
    sym.replace(' ', '_').replace('-', '_').to_lower();
    return Rice::Symbol(sym);
}

// VALUE val = obj.value();
// auto unicode = StringValueCStr(val);

Rice::Object icu_set_codepoint_internal(Rice::Object &self, const Rice::Object &value)
{
    VALUE rb_val = value.value();
    Rice::Object ret = to_ruby(Qnil);

    if (RB_TYPE_P(rb_val, T_STRING))
    {
        UnicodeString val(StringValueCStr(rb_val));
        if (val.size() == 0)
        {
            // set 0x0 as code point on empty string input
            ret = to_ruby(0);
            self.iv_set("@codepoint", ret);
        }
        else
        {
            // set first character of input string as code point
            ret = to_ruby(val.at(0)());
            self.iv_set("@codepoint", ret);
        }
    }
    else if (RB_TYPE_P(rb_val, T_BIGNUM) || RB_TYPE_P(rb_val, T_FIXNUM))
    {
        // set integer value as code point
        ret = value;
        self.iv_set("@codepoint", value);
    }
    else
    {
        // keep object value unchanged and throw a Ruby exception on incompatible data type
        throw Rice::Exception(rb_eTypeError, "ICU::Char: codepoint must be either an Integer or a String (first letter)");
    }

    return ret;
}

void icu_init(Rice::Object self, Rice::Object value)
{
    (void) icu_set_codepoint_internal(self, value);
}

Rice::Object icu_get_codepoint(Rice::Object self)
{
    return self.iv_get("@codepoint");
}

Rice::Object icu_set_codepoint(Rice::Object self, Rice::Object value)
{
    return icu_set_codepoint_internal(self, value);
}

#define ICU_PROPERTY_METHOD(name) \
    Rice::Object icu_##name(Rice::Object self) { \
        return icu_ret(self, &Char::name); \
    }

ICU_PROPERTY_METHOD(is_space)
ICU_PROPERTY_METHOD(is_uppercase)
ICU_PROPERTY_METHOD(is_lowercase)
ICU_PROPERTY_METHOD(is_titlecase)
ICU_PROPERTY_METHOD(is_alphabetic)
ICU_PROPERTY_METHOD(is_alphanumeric)
ICU_PROPERTY_METHOD(is_control_character)
ICU_PROPERTY_METHOD(is_blank)
ICU_PROPERTY_METHOD(is_punctuation)
ICU_PROPERTY_METHOD(is_printable)
ICU_PROPERTY_METHOD(is_digit)
ICU_PROPERTY_METHOD(is_non_character)
ICU_PROPERTY_METHOD(is_high_surrogate)
ICU_PROPERTY_METHOD(is_low_surrogate)
ICU_PROPERTY_METHOD(is_surrogate)
ICU_PROPERTY_METHOD(requires_surrogates)
ICU_PROPERTY_METHOD(is_cased)
ICU_PROPERTY_METHOD(is_ltr)
ICU_PROPERTY_METHOD(is_rtl)

#define ICU_CHARACTER_CHANGE(name) \
    Rice::Object icu_##name(Rice::Object self) { \
        return icu_char_set(self, &Char::name); \
    }

ICU_CHARACTER_CHANGE(to_high_surrogate)
ICU_CHARACTER_CHANGE(to_low_surrogate)
ICU_CHARACTER_CHANGE(to_uppercase)
ICU_CHARACTER_CHANGE(to_lowercase)
ICU_CHARACTER_CHANGE(to_titlecase)
ICU_CHARACTER_CHANGE(fold_case)

Rice::Object icu_get_char(Rice::Object self)
{
    auto cp = object_to_codepoint(self);
    return MakeRubyUTF8String(Char::utf8(cp).c_str());
}

Rice::Object icu_get_char_name(Rice::Object self)
{
    auto cp = object_to_codepoint(self);
    return MakeRubyUTF8String(Char::name(cp).c_str());
}

Rice::Object icu_get_script(Rice::Object self)
{
    return icu_get_sym(self, &Char::script_name);
}

Rice::Object icu_get_script_name(Rice::Object self)
{
    auto cp = object_to_codepoint(self);
    return MakeRubyUTF8String(Char::script_name(cp).c_str());
}

Rice::Object icu_get_script_shortname(Rice::Object self)
{
    return icu_get_sym(self, &Char::script_shortname);
}

Rice::Object icu_get_category(Rice::Object self)
{
    auto cp = object_to_codepoint(self);
    return to_ruby(static_cast<int32_t>(Char::category(cp)));
}

//Rice::Object icu_get_category_name(Rice::Object self)
//{
//}

Rice::Object icu_get_block(Rice::Object self)
{
    auto cp = object_to_codepoint(self);
    return to_ruby(static_cast<int32_t>(Char::block(cp)));
}

//Rice::Object icu_get_block_name(Rice::Object self)
//{
//}

Rice::Object icu_get_direction(Rice::Object self)
{
    auto cp = object_to_codepoint(self);
    return to_ruby(static_cast<int32_t>(Char::direction(cp)));
}

//Rice::Object icu_get_direction_name(Rice::Object self)
//{
//}


// Load this module from Ruby using:
//   require "icu"

extern "C" {

void Init_icu_bindings()
{
    // Global Scope: Module ICU
    auto icu = Rice::define_module("ICU");
    //icu.define_module_function("test", &test_function);

    // Module Scope (ICU): Class Char
    auto icu_char = icu.define_class("Char");
    icu_char.define_method("initialize", &icu_init, Rice::Arg("codepoint") = to_ruby(0));

    icu_char.define_method("codepoint",  &icu_get_codepoint);
    icu_char.define_method("codepoint=", &icu_set_codepoint);
    icu_char.define_method("cp",         &icu_get_codepoint);
    icu_char.define_method("cp=",        &icu_set_codepoint);

    icu_char.define_method("char",       &icu_get_char);
    icu_char.define_method("name",       &icu_get_char_name);

    icu_char.define_method("is_space?",             &icu_is_space);
    icu_char.define_method("is_uppercase?",         &icu_is_uppercase);
    icu_char.define_method("is_lowercase?",         &icu_is_lowercase);
    icu_char.define_method("is_titlecase?",         &icu_is_titlecase);
    icu_char.define_method("is_alphabetic?",        &icu_is_alphabetic);
    icu_char.define_method("is_alphanumeric?",      &icu_is_alphanumeric);
    icu_char.define_method("is_control_character?", &icu_is_control_character);
    icu_char.define_method("is_blank?",             &icu_is_blank);
    icu_char.define_method("is_punctuation?",       &icu_is_punctuation);
    icu_char.define_method("is_printable?",         &icu_is_printable);
    icu_char.define_method("is_digit?",             &icu_is_digit);
    icu_char.define_method("is_non_character?",     &icu_is_non_character);
    icu_char.define_method("is_high_surrogate?",    &icu_is_high_surrogate);
    icu_char.define_method("is_low_surrogate?",     &icu_is_low_surrogate);
    icu_char.define_method("is_surrogate?",         &icu_is_surrogate);
    icu_char.define_method("requires_surrogates?",  &icu_requires_surrogates);
    icu_char.define_method("is_cased?",             &icu_is_cased);
    icu_char.define_method("is_ltr?",               &icu_is_ltr);
    icu_char.define_method("is_rtl?",               &icu_is_rtl);

    icu_char.define_method("to_high_surrogate",     &icu_to_high_surrogate);
    icu_char.define_method("to_low_surrogate",      &icu_to_low_surrogate);
    icu_char.define_method("to_uppercase",          &icu_to_uppercase);
    icu_char.define_method("to_lowercase",          &icu_to_lowercase);
    icu_char.define_method("to_titlecase",          &icu_to_titlecase);
    icu_char.define_method("fold_case",             &icu_fold_case);

    icu_char.define_method("script",                &icu_get_script);
    icu_char.define_method("script_name",           &icu_get_script_name);
    icu_char.define_method("script_shortname",      &icu_get_script_shortname);

    icu_char.define_method("category",              &icu_get_category);
    icu_char.define_method("block",                 &icu_get_block);
    icu_char.define_method("direction",             &icu_get_direction);
}

}
