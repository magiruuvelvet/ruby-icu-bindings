#ifndef LIST_HPP
#define LIST_HPP

#include <string>
#include <list>
#include <algorithm>
#include <cinttypes>

#include "IteratorUtils.hpp"

// wrapper around std::list<T> to add useful methods
// note: "this->" is explicitly required due to the usage of a template
//       fails to find base class methods otherwise
template<typename T>
class List : public std::list<T>
{
public:
    using std::list<T>::list;

    using size_type = typename std::list<T>::size_type;
    using index = std::int64_t;
    static const index npos = -1;

    using iterator = typename std::list<T>::iterator;
    using const_iterator = typename std::list<T>::const_iterator;
    using value_type = typename std::list<T>::value_type;
    using difference_type = typename std::list<T>::difference_type;

    // default constructor
    List() = default;

    // default copy assignment
    List &operator=(const List&) = default;

    // copy assign from base class
    inline List &operator=(const std::list<T> &vec)
    { *this = vec; return *this; }

    // operator*, direct access to raw data -> *myVector
    inline const T *operator*() const
    { return this->data(); }

    // concatenate operator, join multiple vectors dynamically
    inline List<T> operator+(const List<T> &other) const
    {
        // deep copy of self
        auto copy = *this;
        copy.insert(copy.end(), other.begin(), other.end());
        return std::move(copy);
    }

    // stream<< operator (append using streams)
    inline List<T> &operator<< (const T &val)
    { this->append(val); return *this; }
    inline List<T> &operator<< (const List<T> &other)
    { this->append(other); return *this; }

    // join List with an optimal element delimiter
    //  > requires the operator+=() to be implemented in T
    // on integral types this will just sum up all elements
    // if T doesn't support delimiters, just keep it empty to skip
    inline T join(const std::string &delim = std::string()) const
    {
        T _merge;
        for (auto&& elem : *this)
        {
            delim.empty() ?
                _merge += elem
              : _merge += elem + delim;
        }
        if (!delim.empty())
        {
            _merge.replace(_merge.size() - delim.size(), delim.size(), "");
        }
        return _merge;
    }

    // convenience contains method, wraps std::find()
    inline bool contains(const T &other) const
    { return std::find(this->begin(), this->end(), other) != this->end(); }

    // remove first element
    inline void remove_first()
    { this->erase(this->begin()); }

    // remove last element
    inline void remove_last()
    { this->erase(--this->end()); }

    // alias for emplace_back()
    inline void append(const T &other)
    { this->emplace_back(other); }

    // convenience multi append, insert List<T> at end
    inline void append(const List<T> &other)
    { for (auto&& elem : other)
          this->emplace_back(elem); }

    // alias for insert(begin, ...)
    inline void prepend(const T &other)
    { std::list<T>::insert(this->begin(), other); }

    // convenience multi prepend, insert List<T> at begin
    inline void prepend(const List<T> &other)
    { auto pos = 0;
      for (auto&& elem : other)
      {
          std::list<T>::insert(this->begin() + pos, elem);
          ++pos;
      }
    }

    // insert element at index
    inline auto insert(size_t i, const T &value)
    { return std::list<T>::insert(this->begin() + i, value); }

    // forward other insert methods
    inline auto insert(const_iterator pos, value_type &&value)
    { return std::list<T>::insert(pos, value); }
    inline auto insert(const_iterator pos, const value_type &value)
    { return std::list<T>::insert(pos, value); }
    inline auto insert(const_iterator pos, std::initializer_list<value_type> l)
    { return std::list<T>::insert(pos, l); }
    inline auto insert(const_iterator pos, size_type n, const value_type &value)
    { return std::list<T>::insert(pos, n, value); }

    // truncate List<T>
    inline List<T> mid(size_type from) const
    {
        if (from == 0 || from > this->size())
            return *this;
        List<T> copy;
        std::copy_n(this->begin() + from, this->size() - from, std::back_inserter(copy));
        return copy;
    }

    // removes duplicate elements from the List<T>
    //  T must have operator= and operator!= implemented
    inline List<T> &remove_duplicates()
    {
        IteratorUtils::RemoveDuplicates(*this);
        return *this;
    }
};

#endif // LIST_HPP
