#ifndef NUMBER_HPP
#define NUMBER_HPP

#include <type_traits>
#include <algorithm>
#include <string>

// musl C lib
using ushort = unsigned short;
using uint = unsigned int;

class Number final
{
    Number() = delete;

public:
    //
    // get the amount of digits of a number (only supports integral types)
    //
    template<typename T, typename = std::enable_if<std::is_integral<T>::value>>
    static unsigned int GetIntLength(T _int, const short &base = 10)
    {
        unsigned int count{0U};
        do {
             ++count;
             _int /= base;
        } while (_int);
        return count;
    }

    // NumberType: data type of the number
    // base: input base to use
    // StringType: string type where the number is stored
    // DataMethod: member function which converts the string into a UTF-8 const char*
    //
    // throws std::invalid_argument if no conversion could be performed
    // throws std::out_of_range if the string value doesn't fit into the data type

    // 32-bit signed
    template<typename NumberType, ushort base, typename StringType, typename DataMethod, typename Enable =
             std::enable_if_t<std::is_same<int, NumberType>::value>>
    struct toInt {
        NumberType operator()(const StringType &str, DataMethod data) const {
            return std::stoi((str.*data)(), 0, base);
        }
    };

    // 32-bit unsigned
    template<typename NumberType, ushort base, typename StringType, typename DataMethod, typename Enable =
             std::enable_if<std::is_same<uint, NumberType>::value>>
    struct toUInt {
        NumberType operator()(const StringType &str, DataMethod data) const {
            return static_cast<NumberType>(std::stoul((str.*data)(), 0, base));
        }
    };

    // 64-bit signed
    template<typename NumberType, ushort base, typename StringType, typename DataMethod, typename Enable =
             std::enable_if_t<std::is_same<int64_t, NumberType>::value>>
    struct toInt64 {
        NumberType operator()(const StringType &str, DataMethod data) const {
            return std::stoll((str.*data)(), 0, base);
        }
    };

    // 64-bit unsigned
    template<typename NumberType, ushort base, typename StringType, typename DataMethod, typename Enable =
             std::enable_if_t<std::is_same<uint64_t, NumberType>::value>>
    struct toUInt64 {
        NumberType operator()(const StringType &str, DataMethod data) const {
            return std::stoull((str.*data)(), 0, base);
        }
    };

    // 32-bit floating-point
    template<typename NumberType, typename StringType, typename DataMethod, typename Enable =
             std::enable_if<std::is_same<float, NumberType>::value>>
    struct toFloat {
        NumberType operator()(const StringType &str, DataMethod data) const {
            return std::stof((str.*data)(), 0);
        }
    };

    // 64-bit floating-point
    template<typename NumberType, typename StringType, typename DataMethod, typename Enable =
             std::enable_if<std::is_same<double, NumberType>::value>>
    struct toDouble {
        NumberType operator()(const StringType &str, DataMethod data) const {
            return std::stod((str.*data)(), 0);
        }
    };

    // 128-bit floating-point
    template<typename NumberType, typename StringType, typename DataMethod, typename Enable =
             std::enable_if<std::is_same<long double, NumberType>::value>>
    struct toLongDouble {
        NumberType operator()(const StringType &str, DataMethod data) const {
            return std::stold((str.*data)(), 0);
        }
    };
};

#endif // NUMBER_HPP
