#ifndef VECTOR_HPP
#define VECTOR_HPP

#include <string>
#include <vector>
#include <algorithm>
#include <cinttypes>

#include "IteratorUtils.hpp"

// wrapper around std::vector<T> to add useful methods
// note: "this->" is explicitly required due to the usage of a template
//       fails to find base class methods otherwise
template<typename T>
class Vector : public std::vector<T>
{
public:
    using std::vector<T>::vector;

    using size_type = typename std::vector<T>::size_type;
    using index = std::int64_t;
    static const index npos = -1;

    using iterator = typename std::vector<T>::iterator;
    using const_iterator = typename std::vector<T>::const_iterator;
    using value_type = typename std::vector<T>::value_type;
    using difference_type = typename std::vector<T>::difference_type;

    // default constructor
    Vector() = default;

    // default copy assignment
    Vector &operator=(const Vector&) = default;

    // copy assign from base class
    inline Vector &operator=(const std::vector<T> &vec)
    { *this = vec; return *this; }

    // operator*, direct access to raw data -> *myVector
    inline const T *operator*() const
    { return this->data(); }

    // concatenate operator, join multiple vectors dynamically
    inline Vector<T> operator+(const Vector<T> &other) const
    {
        // deep copy of self
        auto copy = *this;
        copy.insert(copy.end(), other.begin(), other.end());
        return std::move(copy);
    }

    // stream<< operator (append using streams)
    inline Vector<T> &operator<< (const T &val)
    { this->append(val); return *this; }
    inline Vector<T> &operator<< (const Vector<T> &other)
    { this->append(other); return *this; }

    // join Vector with an optimal element delimiter
    //  > requires the operator+=() to be implemented in T
    // on integral types this will just sum up all elements
    // if T doesn't support delimiters, just keep it empty to skip
    inline T join(const std::string &delim = std::string()) const
    {
        T _merge;
        for (auto i = 0; i < this->size(); i++)
        {
            // last element
            if (i == this->size() -1 )
            {
                _merge += this->at(i);
            }
            else
            {
                delim.empty() ?
                    _merge += this->at(i)
                  : _merge += this->at(i) + delim;
            }
        }
        return _merge;
    }

    // convenience contains method, wraps std::find()
    inline bool contains(const T &other) const
    { return std::find(this->begin(), this->end(), other) != this->end(); }

    // find first index starting from [from] of element, returns npos if not found
    inline index index_of(const T &elem, size_t from = 0) const
    { const auto &iter = std::find_if(this->begin() + static_cast<difference_type>(from), this->end(),
                                      [&](const auto &current) {
          return current == elem;
      });
      index index = std::distance(this->begin(), iter);
      return index == this->size() ? -1 : index;
    }

    // find last index starting from [from] of element, returns npos if not found
    inline index last_index_of(const T &elem, size_t from = 0) const
    {
        if (from >= this->size())
            from = this->size() - 1;

        const T *b = this->begin().base() + from;
        const T *n = this->end().base();
        while (n != b) {
            if (*--n == elem)
                return (n - b) + from;
        }
        return -1;
    }

    // find last index in the range 0~len of element, returns npos if not found
    inline index __last_index_of(size_t len, const T &elem) const
    {
        Vector<T> copy;
        std::copy_n(this->begin(), len, std::back_inserter(copy));
        return copy.last_index_of(elem);
    }

    // get first element in vector
    inline const T &first() const
    { return this->at(0); }

    // get last element in vector
    inline const T &last() const
    { return this->at(this->size() - 1); }

    // replaces element at index
    inline void replace(size_t i, const T &value)
    { this->at(i) = value; }

    // remove first element
    inline void remove_first()
    { this->erase(this->begin()); }

    // remove last element
    inline void remove_last()
    { this->erase(--this->end()); }

    // remove element at index
    inline void remove_at(size_t i)
    { this->erase(this->begin() + i); }

    // alias for emplace_back()
    inline void append(const T &other)
    { this->emplace_back(other); }

    // convenience multi append, insert Vector<T> at end
    inline void append(const Vector<T> &other)
    { for (auto&& elem : other)
          this->emplace_back(elem); }

    // alias for insert(begin, ...)
    inline void prepend(const T &other)
    { std::vector<T>::insert(this->begin(), other); }

    // convenience multi prepend, insert Vector<T> at begin
    inline void prepend(const Vector<T> &other)
    { auto pos = 0;
      for (auto&& elem : other)
      {
          std::vector<T>::insert(this->begin() + pos, elem);
          ++pos;
      }
    }

    // insert element at index
    inline auto insert(size_t i, const T &value)
    { return std::vector<T>::insert(this->begin() + i, value); }

    // forward other insert methods
    inline auto insert(const_iterator pos, value_type &&value)
    { return std::vector<T>::insert(pos, value); }
    inline auto insert(const_iterator pos, const value_type &value)
    { return std::vector<T>::insert(pos, value); }
    inline auto insert(const_iterator pos, std::initializer_list<value_type> l)
    { return std::vector<T>::insert(pos, l); }
    inline auto insert(const_iterator pos, size_type n, const value_type &value)
    { return std::vector<T>::insert(pos, n, value); }

    // truncate Vector<T>
    inline Vector<T> mid(size_type from) const
    {
        if (from == 0 || from > this->size())
            return *this;
        Vector<T> copy;
        std::copy_n(this->begin() + from, this->size() - from, std::back_inserter(copy));
        return copy;
    }

    // removes duplicate elements from the Vector<T>
    //  T must have operator= and operator!= implemented
    inline Vector<T> &remove_duplicates()
    {
        IteratorUtils::RemoveDuplicates(*this);
        return *this;
    }
};

#endif // VECTOR_HPP
