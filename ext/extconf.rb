require "mkmf-rice"

extension_name = "icu_bindings"

dir_config(extension_name)

$srcs = [
    "icu-bindings/ICUBindings.cpp",
    "icu-bindings/Char.cpp",
    "icu-bindings/UnicodeString.cpp",
]

# only when installing from "bundle install": remove "register" error and put it back to warning level
# works fine when compiling with rake
$CFLAGS += " -O3 -march=native -DNDEBUG -D_NDEBUG -Wno-error=register -Wno-deprecated-declarations"
$CXXFLAGS += " -std=c++1z -O3 -march=native -DNDEBUG -D_NDEBUG -Wno-error=register -Wno-deprecated-declarations"
$INCFLAGS << " -I$(srcdir)/icu-bindings"

# link to ICU UC lib
have_library("icuuc")

$VPATH << "$(srcdir)/icu-bindings"

create_makefile(extension_name)
