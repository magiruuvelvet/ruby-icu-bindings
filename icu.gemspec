Gem::Specification.new do |s|
  s.name        = "icu"
  s.version     = "1.0.0"
  s.licenses    = ["MIT"]
  s.summary     = "Minimal ICU bindings for Ruby"
  #s.description = ""
  s.authors     = ["マギルゥーベルベット"]
  #s.email       = ""
  s.files       = ["lib/icu.rb"]
  #s.homepage    = ""
  #s.metadata    = { "source_code_uri" => "" }
  s.extensions = %w[ext/extconf.rb]

  # HACK: install build dependencies for native extension
  #s.add_development_dependency "rice"
  s.add_runtime_dependency "rice"
end
