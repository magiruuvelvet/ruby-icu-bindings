require_relative "../icu"

c = ICU::Char.new

def test_classes(char)
    puts "codepoint: #{char.codepoint} => #{char.char}, #{char.script.inspect}, #{char.script_name.inspect}, #{char.script_shortname.inspect}"
    puts "name:      #{char.name}"
    puts "category:  #{char.category}"
    puts "block:     #{char.block}"
    puts "direction: #{char.direction}"
    puts "blank:     #{char.is_blank?}"
    puts "digit:     #{char.is_digit?}"
    puts "space:     #{char.is_space?}"
end

test_classes(c)

c.cp = 0x20
test_classes(c)

c.cp = "A"
test_classes(c)

c.cp = "Ü"
test_classes(c)

c.cp = "あ"
test_classes(c)

puts "==="

c.cp = "a"
puts c.to_uppercase.inspect
puts c.char
puts c.to_lowercase.inspect
puts c.char
